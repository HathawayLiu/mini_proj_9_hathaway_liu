## Mini Project 9: Streamlit App with HuggingFace LLM
## Project Description
This project requires us to create a web app using Streamlit, along with a LLM in HuggingFace. In this project, I use a [GPT2 model](https://huggingface.co/openai-community/gpt2) by OpenAi in HuggingFace. In this app, when you type in some random words, it will generates some results with full sentences in certain length.

## Packages Installation
For this specific project, we are required to install corresponding packages. You can use the following commands to install:
```python
pip install transformers
pip install tensorflow
pip install streamlit
pip install tf-keras
pip install torch
```
For successful deployment, I also add a `requirements.txt` file for installing the above packages.
## Detailed steps
1. Create a empty python file. Here I named it `streamlit_app.py`
2. Start your implementation of Streamlit App, along with the model usage that you can find in HuggingFace. To make the web app fancier, I add some corresponding html/CSS styling.
3. Go to https://streamlit.io/ to create a new acocount.
4. By running the following command:  
```python
streamlit run streamlit_app.py ##<YOUR .py file name>
```
  A window will pop up that shows that app is running. Or you can find out the link by following the output that displays on the terminal:
![terminal](https://gitlab.com/HathawayLiu/mini_proj_9_hathaway_liu/-/wikis/uploads/a097c1dad090bbf577c40afb9765c0d7/Screenshot_2024-04-07_at_2.54.51_PM.png)

Example of my app running looks like the following:
![app](https://gitlab.com/HathawayLiu/mini_proj_9_hathaway_liu/-/wikis/uploads/8c96449d0f8f1aef8f85710bb1392130/Screenshot_2024-04-06_at_9.31.54_PM.png)

## Test and Deploy
To deploy the Streamlit app on the Streamlit, you could follow these steps:
1. Go to main page of Streamlit. On top right there should be a `New App` button. Click on it and choose `Use existing repo`.
2. Since Streamlit currently doesn't support Gitlab, you can make a copy of your files in Gitlab to Github. Fill in the corresponding information for the repo following the instructions on the website.
3. Then deploy it. The website should start to deploy like the following:
![deploy](https://gitlab.com/HathawayLiu/mini_proj_9_hathaway_liu/-/wikis/uploads/fd8256599d9b39df555b9b479bcd5bce/Screenshot_2024-04-07_at_3.03.38_PM.png)
4. When you succesfully deploy it, there should be a link allow you to visit like following: https://mini-proj-9-kus2oenxsk2dllukhrwdde.streamlit.app/

## Demo Video
![clip](https://gitlab.com/HathawayLiu/mini_proj_9_hathaway_liu/-/wikis/uploads/802c0203fe2b580e1aa73d90de3402eb/Clip_1.mov)